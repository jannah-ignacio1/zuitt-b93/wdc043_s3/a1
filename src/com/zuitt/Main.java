package com.zuitt;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args){

        int ans = 1;
        int i = 1;

        Scanner in = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed.");

        try {
            int num = in.nextInt();

            if (num >= 0) {
                while (i <= num) {
                    ans = ans * i;
                    i++;
                }
                System.out.println("The factorial of " + num + " is " + ans);
            } else if (num < 0) {
                System.out.println("Input a non-negative number");
            }
        }catch(InputMismatchException e) {
            System.out.println("Input a valid number.");
        } catch(Exception e) {
                System.out.println("Invalid input.");
        }
    }
}
